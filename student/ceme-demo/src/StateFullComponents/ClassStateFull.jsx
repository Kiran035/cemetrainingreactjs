import React from 'react'

class ClassStateFull extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state = {
            name:"Kiran",
            company:"Allstate",
            count:0
        }
    }

    increase = () =>{
        //this.setState({count:this.state.count+1})
        //Not recomended approch use arrow function to update state always

        this.setState((preState)=>{
            return{count:preState.count+1}
        })
    }
    render(){
        return(
            <>
                <h1> Class State Component - Hello {this.state.name} from {this.state.company}</h1>
                <button onClick={this.increase} > Click - {this.state.count} </button>
            </>
        )
    }

}

export default ClassStateFull;