import {useState,useEffect} from 'react'
const FunctionHook = (/*props*/) =>{
    const [name, setName] = useState("Kiran")
    const [company, setCompany] = useState("Allstate")
    const [count, setCount] = useState(0)
    return(
        <>
        <h1> Function Hook - Welcome {name} from {company} </h1>
        <button onClick={() => setCount(count+1)}> Click - {count} </button>
            </> 
    );
}

export default FunctionHook