import { render } from '@testing-library/react'
import React from 'react'
class ClassComponent extends React.Component
{
    render(){
        return(
        <h1> Class Component - Hello {this.props.name} from {this.props.company}</h1>
        )
    }

}

export default ClassComponent;