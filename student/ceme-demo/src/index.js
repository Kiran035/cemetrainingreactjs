import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HelloFunction from './components/1_simple/functionComponent'
import HelloFunctionDe from './components/1_simple/functionComponentDestructure'
import ArrowFunction from './components/1_simple/ArrowFunction'
import ClassComponent from './components/1_simple/ClassComponent'
import ClassStateFull from './StateFullComponents/ClassStateFull'
import FunctionHook from './StateFullComponents/FunctionHooks'
ReactDOM.render(
  <React.StrictMode>
    <HelloFunction name="Kiran" company="Allstate" />     
    <HelloFunctionDe name="Kiran" company="Allstate" />   
    <ArrowFunction name="Kiran" company="Allstate" /> 
    <ClassComponent name="Kiran" company="Allstate" /> 
    <ClassStateFull />  
    <FunctionHook />  
  </React.StrictMode>,
  //document.getElementById('root')
  document.querySelector("#root")
);


