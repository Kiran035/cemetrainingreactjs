import React from 'react'

import EmplyeeCard from './EmplyeeCard'
import TitleContainer from './TitleContainer'
const App = (props) =>{
    const empData = [
        {
            id: 0,
            name: "Jhon",
            age: 32,
            email: "Test@gamil.com",
            salary: 10000
            },
            
            {
            id: 1232,
            name: "Dee",
            age: 50,
            email: "d@i.ie",
            salary: 1234
            },
            {
            id: 1234,
            name: "Mark",
            age: 32,
            email: "Test@gamil.com",
            salary: 10000
            }
            
    ]

    

    return (<>
    
        <TitleContainer />
        <div className="card-group">
            {
                empData.map((emplyee,i)=>(
                    <EmplyeeCard key={i} data={emplyee}/>
                ))
            }            
        </div>

        </>
    )
}

export default App