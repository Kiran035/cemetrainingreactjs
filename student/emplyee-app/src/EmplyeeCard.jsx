import React, {useState} from 'react'
import EmplyeeHeader from './EmplyeeHeader'
import EmplyeeDetails from './EmplyeeDetails'
import ShowDetails from './ShowDetails'
const EmplyeeCard = (props) => {
    const [status,setStatus] = useState(false)
    return(
       
            <div className="card border border-success mb-3 mx-auto"  style={{maxWidth: '18rem'}}>
                <EmplyeeHeader name={props.data.name}/>            
                <EmplyeeDetails data={props.data} status={status}/>
                <ShowDetails status={status} handleClick={setStatus}/>
            </div>
            
    )
}


export default EmplyeeCard