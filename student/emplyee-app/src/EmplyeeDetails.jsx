import React from 'react'

const EmplyeeDetails = (props) =>{
    return(
        props.status ?<>
         <div className="card-body text-success">
            <h5 className="card-title">Details oF Emplyee</h5>
            <p className="card-text">
                <div>{props.data.age}</div>
                <div>{props.data.email}</div>
                <div>{props.data.salary}</div>
            </p>
        </div>
            
        </> : <div className="card-body text-success"> Click To Show the data</div>
    )
}

export default EmplyeeDetails