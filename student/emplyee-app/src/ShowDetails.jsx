import React from 'react'

const ShowDetails = (props) =>{
    const handleClick = () => {
        props.handleClick(props.status?false:true)
    }
    return(

        <div className="card-footer bg-transparent border-success"> 
            <button onClick={handleClick}>{props.status?"Hide":"Show"} Details</button>
        </div>
    
    )
}

export default ShowDetails