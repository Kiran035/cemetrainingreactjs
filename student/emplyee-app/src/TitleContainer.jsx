import React from 'react'

const TitleContainer = () =>{
    return (
        <section class="jumbotron text-center">
            <div class="container">
            <h1 class="jumbotron-heading">Emplyee Data</h1>
            <p class="lead text-muted">Employee Database Management Open HRM’s centralized database lets you organize and manage all the employee data from a single point. With this finest employee database management software, the end user can easily track the Leaves, Attendances, Timesheets, Appraisals, Expenses, and more from employee master form.</p>
            <p>
                <a href="#" class="btn btn-primary my-2">Main call to action</a>
                <a href="#" class="btn btn-secondary my-2">Secondary action</a>
            </p>
            </div>
        </section>
    )
}

export default TitleContainer