import React,{useState} from 'react'
import AlbumTitle from './AlbumTitle'
import AlbumArtist from './AlbumArtist'
import AlbumTracks from './AlbumTracks'
import Toggle from './Toggle'

const Album = (props) =>{
    const [visible,setVisible] = useState(false);
    const handleToggle = () =>{
        setVisible(visible?false:true)
    }

    return(
                    <section >
                        <AlbumTitle  title={props.data.title} />
                        <AlbumArtist artist={props.data.artist} />
                        <Toggle handleClick={handleToggle}/>
                        <AlbumTracks track = {props.data.tracks}  visible={visible} />
                    </section>
            
              
    )
}

export default Album 