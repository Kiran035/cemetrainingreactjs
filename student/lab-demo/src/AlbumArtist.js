 import React from 'react'

 const AlbumArtist = (props) =>{
    return <h3 className="album-artist">{props.artist}</h3>
 }

 export default AlbumArtist