import React from 'react'
import "./App.css"
import Album from './Album'
const App = () =>{
    const album = [
        {
            title: "album 1",
            artist: "artist 1",
            tracks: ["tr 11","tr 12","tr 13"]
        },
        {
            title: "album 2",
            artist: "artist 2",
            tracks: ["tr 21","tr 22","tr 23"]
        },
        {
            title: "album 3",
            artist: "artist 3",
            tracks: ["tr 31","tr 32","tr 33"]
        }
        
    ]
    return(

        <div className="app">
            <div className="album">
            {
                album.map((item,i) =>(<Album key={i} data ={item} /> ))
            }
            </div>
        </div>
    )
}

export default App